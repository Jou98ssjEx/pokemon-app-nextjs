import React, { FC } from 'react'

import { GetStaticProps, GetStaticPaths } from 'next';

import { pokeApi } from '../../api';
import { MainLayout } from '../../components/layouts';
import { Pokemon } from '../../interfaces/pokemonFull';
import { ViewPokemons } from '../../components/pokemons';
import { getPokemonInfo } from '../../utils';
import { redirect } from 'next/dist/server/api-utils';

interface Props {
  pokemon: Pokemon
}

 const PokemonPage: FC <Props> = ( { pokemon } ) => {

  // let a = localStorage.getItem('favorites');
  // console.log(a);
  // let b = localPokemons.existInFavorites(pokemon.id);
  // console.log(b);
 
  return (
    <div>
      <MainLayout title={`${pokemon.name}`}>
     
       <ViewPokemons pokemon={ pokemon} />

      </MainLayout>
    </div>
  )
}


export const getStaticPaths: GetStaticPaths = async (ctx) => {
  // console.log({al: ctx});
  //  const pokemon151 = [...Array(151)].map( ( value, idx) => `${idx + 1}`)
  const pokemon151 = [...Array(151)].map( ( value, idx) => { 
    // console.log({idx});
    return `${idx + 1}`
  })

  return {
    paths: pokemon151.map( id =>({
      params: { id }
    })),
    // fallback: false // false or 'blocking'
    fallback: 'blocking', // para que pase y no de un 404
  };
}


export const getStaticProps: GetStaticProps = async (ctx) => {
  // console.log({ctx});
  const { id } = ctx.params as { id: string}

  const pokemon = await getPokemonInfo(id);

  if (!pokemon){
    
    console.log('redired');
    // redirect( `/`)
    return{
      redirect:{
        destination: '/',
        permanent: false
      }
    }
  }

  return {
    props: {
      pokemon,
    },
    revalidate: 18400 // en: MS 60seg * 60min * 24h
  }
}

export default PokemonPage