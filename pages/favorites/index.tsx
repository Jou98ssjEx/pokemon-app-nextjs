import { Card, Grid } from '@nextui-org/react';
import React, { useEffect } from 'react'
import {useState} from 'react';

import { MainLayout } from '../../components/layouts'
import { NoPokemons } from '../../components/ui'
import { localPokemons } from '../../utils';

import { FavoritePokemons } from '../../components/pokemons';

 const Favorites = () => {

  const [favoritePokemons, setFavoritePokemons] = useState<number[]>([]);

  useEffect(() => {
    setFavoritePokemons( localPokemons.pokemons() );
  }, [])
  
  return (
    <div>
        <MainLayout>

          {
            !favoritePokemons && 
                  <NoPokemons />
          }

          <FavoritePokemons pokemons={ favoritePokemons }  />

        </MainLayout>
    </div>
  )
}

export default Favorites