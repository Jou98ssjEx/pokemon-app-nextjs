import { FC } from 'react'

import { GetStaticProps, GetStaticPaths } from 'next'
import { pokeApi } from '../../api'
import { PokeListResponse, Pokemon } from '../../interfaces'
import { MainLayout } from '../../components/layouts'
import { ViewPokemons } from '../../components/pokemons'
import { getPokemonInfo } from '../../utils'

interface Props {
    pokemon: Pokemon
  }

 const PokemonByNamePage: FC <Props> = ( { pokemon }) => {

    // console.log(pokemon.name);
  return (
    <div>
        <MainLayout title={`${pokemon.name}`}>
     
            <ViewPokemons pokemon={ pokemon} />

        </MainLayout>
    </div>
  )
}




export const getStaticPaths: GetStaticPaths = async (ctx) => {
    const { data } = await pokeApi.get<PokeListResponse>(`/pokemon?limit=151`);

    const pokeName: string[] = data.results.map( pokemon => pokemon.name);

    return {
        paths: pokeName.map( name =>({
        params: { name }
        })),
        // fallback: false // false or 'blocking'
        fallback: 'blocking', // para que pase y no de un 404
    }
}


export const getStaticProps: GetStaticProps = async (ctx) => {
    // console.log({ctx});
    const { name } = ctx.params as { name: string}
  
    const pokemon = await getPokemonInfo( name );
    
    if (!pokemon){
    
      console.log('redired');
      return{
        redirect:{
          destination: '/',
          permanent: false
        }
      }
      // redirect( `/`)
    }
    return {
      props: {
        pokemon,
      },
      revalidate: 18400 // en: MS 60seg * 60min * 24h
    }
}

export default PokemonByNamePage