import { Button, Card, Grid, Row, Text } from '@nextui-org/react'
import type { NextPage, GetStaticProps } from 'next'
import { MainLayout } from '../components/layouts'
import React from 'react';
import {pokeApi} from '../api';
import { PokeListResponse, SmallPoke } from '../interfaces';
import { PokemonCard } from '../components/pokemons/PokemonCard';

// type MainLayout = {
//   title: string,
// }

interface Props {
  pokemons: SmallPoke[];
}

const Home: NextPage <Props> = ({pokemons}) => {
  // console.log(pokemons); 
  return (
    <>
      <MainLayout title={ `Inicio`}>

        <Grid.Container gap={2} justify='flex-start'>
          {
            pokemons.map( (poke) => (
             <PokemonCard  
              key={poke.uid}
              pokemon={poke}
              // {...poke}
             />
            ))
          }
        </Grid.Container>
       


        {/*<Button color={'gradient'}>New add</Button> */}
      </MainLayout>

    </>
  )
}



export const getStaticProps: GetStaticProps = async (ctx) => {
  // const { data } = await  // your fetch function here 

  const { data }= await pokeApi.get<PokeListResponse>('/pokemon?limit=151');
  // console.log(data);
  // https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/132.svg

  const newPoke: SmallPoke[] = data.results.map( (pokemon, idx) => ({ 
    uid: `${idx +1}`, 
    name: pokemon.name, 
    img: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${idx +1 }.svg`,
    url: pokemon.url
  }))

  return {
    props: {
      // pokemons: data.results
      pokemons: newPoke
    }
  }
}

export default Home
