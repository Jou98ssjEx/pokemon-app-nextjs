import Head from 'next/head'
import React, { FC } from 'react'
import { Nabvar } from '../ui';

interface Props {
    title?: string,
}

const origin = ( typeof window === 'undefined') ? '' : window.location.origin;

export const MainLayout: FC <Props>  = ( { children, title } ) => {
  return (
    <>
        <Head>
            <title>{ title || 'Pokemon App'}</title>
            <meta name='author' content='Joao García' />
            <meta name='description' content={`Info of ${title} Pokemon App`}/>
            <meta name='keywords' content={`Pokemons ${title}`} />

            {/* Al momento de compartir se visualice cierta info de la app */}
            <meta property="og:title" content={`Pokemons ${title}`}  />
            <meta property="og:description" content={`Info of ${title} Pokemon App`} />
            <meta property="og:image" content={`${origin}/img/banner.png`} />

        </Head>

        <Nabvar/>

        <main>
            { children }
        </main>
    </>
  )
}
