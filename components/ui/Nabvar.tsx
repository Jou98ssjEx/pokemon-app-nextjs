import { Spacer, Text, useTheme, Link } from '@nextui-org/react'
import NextLink from 'next/link'
import Image from 'next/image';
import React from 'react'
// import Link from 'next/link';

export const Nabvar = () => {
    const { theme } = useTheme();
  return (
    <div style={{
        display: 'flex',
        width: '100%',
        flexDirection: 'row',
        alignContent: 'center',
        justifyContent: 'start',
        padding: '0rem .5rem',
        backgroundColor: theme?.colors.gray900.value

    }}>
        <Image 
            src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/132.png`}
            alt='Img Pokemon'
            width={70}
            height={70}

        />

        <NextLink
          href={`/`}
          passHref
        >
          <Link>
            <Text color='white' h2>P</Text>        
            <Text color='white'css={{mt: '.9rem'}} h3>okémon</Text>        
          </Link>
        </NextLink>

        <Spacer css={{flex: 1}} />

        <NextLink
          href={`/favorites`}
          passHref
        >
          <Link>
            <Text color='white' h3>Favoritos</Text>        
          </Link>
        </NextLink>
    </div>
  )
}
