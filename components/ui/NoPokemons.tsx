import { Container, Text } from '@nextui-org/react'
import React from 'react'

export const NoPokemons = () => {
  return (
    <Container>
        <Text h1> No Pokemons </Text>
    </Container>
  )
}

