import React, { FC, useState } from 'react'

import { Button, Card, Container, Grid, Text } from '@nextui-org/react'
import Image from 'next/image'

import confetti from 'canvas-confetti';

import { Pokemon } from '../../interfaces/pokemonFull';
import { localPokemons } from '../../utils';

interface Props {
    pokemon: Pokemon
}

export const ViewPokemons: FC <Props>  = ( { pokemon }) => {

    const [isInFavorites, setIsInFavorites] = useState(localPokemons.existInFavorites(pokemon.id));
    // console.log(isInFavorites);
  
  
    const onToggleFavorites = ( ) => {
      // localFavorites(pokemon.id)
      localPokemons.localFavorites(pokemon.id);
      setIsInFavorites(!isInFavorites);
      // console.log(typeof pokemon.id)
  
      if( isInFavorites ) return;
  
      confetti({
        zIndex: 999,
        particleCount: 100,
        spread: 160,
        angle: -100,
        origin:{
          x: 1,
          y: 0
        }
      })
      
    }
  
  return (
    <Grid.Container css={{ marginTop: '5px' }} gap={ 2 }>
    <Grid xs={ 12 } sm={ 4 } >
      <Card hoverable css={{ padding: '30px' }}>
          <Card.Body>
            <Card.Image 
              src={ pokemon.sprites.other?.dream_world.front_default || '/no-image.png' }
              alt={ pokemon.name }
              width="100%"
              height={ 200 }
            />
          </Card.Body>
      </Card>
    </Grid>

    <Grid xs={ 12 } sm={ 8 }>
      <Card>
        <Card.Header css={{ display: 'flex', justifyContent: 'space-between' }}>
          <Text h1 transform='capitalize'>{ pokemon.name }</Text>

          <Button
            color="gradient"
            ghost={ !isInFavorites }
            onClick={ onToggleFavorites }
          >
            {
              isInFavorites ? 'In Favoritos': 'Saved in Favorites'
            }
          </Button>
        </Card.Header>

        <Card.Body>
          <Text size={30}>Sprites:</Text>

          <Container direction='row' display='flex' gap={ 0 }>
              <Image 
                src={ pokemon.sprites.front_default }
                alt={ pokemon.name }
                width={ 100 }
                height={ 100 }
              />
              <Image 
                src={ pokemon.sprites.back_default }
                alt={ pokemon.name }
                width={ 100 }
                height={ 100 }
              />
              <Image 
                src={ pokemon.sprites.front_shiny }
                alt={ pokemon.name }
                width={ 100 }
                height={ 100 }
              />
              <Image 
                src={ pokemon.sprites.back_shiny }
                alt={ pokemon.name }
                width={ 100 }
                height={ 100 }
              />

          </Container>


        </Card.Body>  


      </Card>
    </Grid>

 </Grid.Container>
  )
}
