import { Card, Grid, Row, Text } from '@nextui-org/react'
import React, { FC } from 'react'
import { SmallPoke } from '../../interfaces'
import { useRouter } from 'next/router';

interface Props {
    pokemon: SmallPoke
}
export const PokemonCard: FC <Props> = ({pokemon}) => {
    const {img, name, uid } = pokemon
    const router = useRouter();
    const handleClick = ( ) => {
        router.push(`/name/${ name }`)
    }
  return (
    <Grid key={uid} xs={6} sm={3} md={2} xl={1}>
        <Card 
            hoverable 
            clickable
            onClick={ handleClick }
        >
            <Card.Body css={{ p: 1}}>
            <Card.Image 
                src={img}
                width='100%'
                height={140}
            />
            </Card.Body>
            <Card.Footer>
            <Row justify='space-between'>
                <Text transform='capitalize'> {name} </Text>
                <Text>#{uid} </Text>
            </Row>
            </Card.Footer>
        </Card>
    </Grid>
  )
}
