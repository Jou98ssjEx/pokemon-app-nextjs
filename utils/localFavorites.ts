
const localFavorites = ( id: number ) => {

    let favorites: number[] = JSON.parse( localStorage.getItem('favorites') || '[]');

    if( favorites.includes(id) ){
        favorites = favorites.filter( pokeID => pokeID !== id);
    } else {
        favorites.push(id);
    }

    // Guardar en localStorage
    localStorage.setItem('favorites', JSON.stringify(favorites));
}

const existInFavorites = ( id: number ) : boolean => {

    // Validar si esta en el lado del cliente
    if (typeof window === 'undefined') return false;

    const favorites: number[] = JSON.parse( localStorage.getItem('favorites') || '[]');

    return favorites.includes(id);
}
 
const pokemons = ( ) :number[] => {

    return JSON.parse( localStorage.getItem('favorites') || `[]`);

}

export default {
    localFavorites,
    existInFavorites,
    pokemons,
}